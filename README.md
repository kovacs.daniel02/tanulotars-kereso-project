# Tanulótárs kereső project

Ez a projekt oldal a **záróvizsgához** készült.

A vizsgaremek 2 fő részből áll:

- [Webes applikáció](Tanulotars_projekt/Web/)
- [Asztali alkalmazás](Tanulotars_projekt/Asztali/)

Készítették:
- *Csontos Dávid*
- *Darányi Balázs*
- *Kovács Dániel*
