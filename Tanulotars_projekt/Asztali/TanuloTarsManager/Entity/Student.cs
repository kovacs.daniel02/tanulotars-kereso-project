﻿using MySql.Data.EntityFrameworkCore.DataAnnotations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TanuloTarsApp.Entity
{
    public class Student
    {
        public int Id { set; get; }
        public string Username { set; get; }
        public int Age { set; get; }
        public string Gender { set; get; }
        public string Location { set; get; }
        public string Email { set; get; }
        public string Degree { set; get; }
        public string Subject { set; get; }
        public string Language { set; get; }
        public string AboutMe { set; get; }

    }
}
