﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TanuloTarsApp.Entity
{
    public class Admin
    {
        public int Id { set; get; }
        public string Username { set; get; }
        public string Password { set; get; }
    }
}
