﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TanuloTarsApp.Entity;
using TanuloTarsApp.Forms;

namespace TanuloTarsApp
{
    public partial class MainForm : Form
    {
        Student selectedObject { get { return (Student)dataGridView1.CurrentRow.DataBoundItem; } }
        StudentContext db;

        public MainForm()
        {
            InitializeComponent();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            db = new StudentContext();

            var adminList = db.Admins.ToList();
            LoginForm loginForm = new LoginForm(adminList);
            loginForm.ShowDialog();
            if (loginForm.loggedAdmin != null)
            {
                this.Text = this.Text + " (Logged as "+loginForm.loggedAdmin.Username.ToString()+")";
            }
            comboBoxSelect.SelectedIndex = 1;
            dataGridView1.DataSource = db.Students.ToList();
        }

        private void dataRefresh()
        {
            if (dataGridView1.CurrentRow == null)
            {
                dataGridView1.DataSource = db.Students.ToList();
            } else
            {
                int index = (int)dataGridView1.CurrentRow.Index;
                dataGridView1.DataSource = db.Students.ToList();
                dataGridView1.Rows[index].Selected = true;
            }

        }

        private void studentInit(Student s)
        {
            s.Username = textBoxUsername.Text;
            if (numericUpDownAge.Text.Length == 0)
            {
                s.Age = 0;
            } else
            {
                s.Age = int.Parse(numericUpDownAge.Text);
            }
            s.Gender = comboBoxGender.Text;
            s.Location = textBoxLocation.Text;
            s.Email = textBoxEmail.Text;
            s.Degree = textBoxDegree.Text;
            s.Subject = textBoxSubject.Text;
            s.Language = textBoxLanguage.Text;
            s.AboutMe = textBoxAboutMe.Text;

        }

        private void clearTextFields()
        {
            textBoxUsername.Text = "";
            numericUpDownAge.Text = "";
            comboBoxGender.Text = "";
            textBoxLocation.Text = "";
            textBoxEmail.Text = "";
            textBoxDegree.Text = "";
            textBoxSubject.Text = "";
            textBoxLanguage.Text = "";
            textBoxAboutMe.Text = "";
        }


        private void btnEdit_Click(object sender, EventArgs e)
        {
            DialogResult dialogResult = 
                MessageBox.Show("Elmented a változtatásokat?" , "Szerkeztés", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {
                studentInit(selectedObject);

                db.Students.Update(selectedObject);
                db.SaveChanges();
                dataRefresh();
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            DialogResult dialogResult = 
                MessageBox.Show("Biztosan törlöd (id:"+selectedObject.Id
                +") "+selectedObject.Username+" felhasználót?", "Törlés", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {
                db.Students.Remove(selectedObject);
                db.SaveChanges();
                dataGridView1.DataSource = db.Students.ToList();
            }
            
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            DialogResult dialogResult =
                MessageBox.Show("Hozzáadod "+textBoxUsername.Text+"-t?", "Hozzáadás", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {
                Student newStudent = new Student();

                studentInit(newStudent);

                db.Students.Add(newStudent);
                db.SaveChanges();
                dataRefresh();
            }
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            clearTextFields();
        }

        private void btnReload_Click(object sender, EventArgs e)
        {
            dataRefresh();
        }

        private void dataGridView1_SelectionChanged(object sender, EventArgs e)
        {
            textBoxUsername.Text = selectedObject.Username;
            numericUpDownAge.Text = selectedObject.Age.ToString();
            comboBoxGender.Text = selectedObject.Gender;
            textBoxLocation.Text = selectedObject.Location;
            textBoxEmail.Text = selectedObject.Email;
            textBoxDegree.Text = selectedObject.Degree;
            textBoxSubject.Text = selectedObject.Subject;
            textBoxLanguage.Text = selectedObject.Language;
            textBoxAboutMe.Text = selectedObject.AboutMe;
        }

        private void textBoxSearch_TextChanged(object sender, EventArgs e)
        {
            switch (comboBoxSelect.SelectedItem.ToString())
            {
                case "Id":
                    dataGridView1.DataSource =
                        db.Students.Where((s) => s.Id.ToString().Contains(textBoxSearch.Text)).ToList();
                    break;
                case "Username":
                    dataGridView1.DataSource =
                        db.Students.Where((s) => s.Username.Contains(textBoxSearch.Text)).ToList();
                    break;
                case "Age":
                    dataGridView1.DataSource =
                        db.Students.Where((s) => s.Age.ToString().Contains(textBoxSearch.Text)).ToList();
                    break;
                case "Gender":
                    dataGridView1.DataSource =
                        db.Students.Where((s) => s.Gender.Contains(textBoxSearch.Text)).ToList();
                    break;
                case "Location":
                    dataGridView1.DataSource =
                        db.Students.Where((s) => s.Location.Contains(textBoxSearch.Text)).ToList();
                    break;
                case "Email":
                    dataGridView1.DataSource =
                        db.Students.Where((s) => s.Email.Contains(textBoxSearch.Text)).ToList();
                    break;
                case "Degree":
                    dataGridView1.DataSource =
                        db.Students.Where((s) => s.Degree.Contains(textBoxSearch.Text)).ToList();
                    break;
                case "Subject":
                    dataGridView1.DataSource =
                        db.Students.Where((s) => s.Subject.Contains(textBoxSearch.Text)).ToList();
                    break;
                case "Language":
                    dataGridView1.DataSource =
                        db.Students.Where((s) => s.Language.Contains(textBoxSearch.Text)).ToList();
                    break;
                case "AboutMe":
                    dataGridView1.DataSource =
                        db.Students.Where((s) => s.AboutMe.Contains(textBoxSearch.Text)).ToList();
                    break;
            };
         
        }

        
    }
}
