﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TanuloTarsApp.Entity;

namespace TanuloTarsApp.Forms
{
    public partial class LoginForm : Form
    {
        List<Admin> admins;
        public Admin loggedAdmin;
        public LoginForm(List<Admin> adminList)
        {
            InitializeComponent();
            admins = adminList;
        }

        private bool validate (String username,String password)
        {
            foreach (Admin a in admins)
            {
                if (username.Equals(a.Username))
                {
                    if (password.Equals(a.Password))
                    {
                        loggedAdmin = a;
                        return true;
                    }
                    MessageBox.Show("Hibás jelszó", "Hiba", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return false;
                }
            }
            MessageBox.Show("Nincs ilyen felhasználó név", "Hiba", MessageBoxButtons.OK, MessageBoxIcon.Error);
            return false;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (validate(textBoxLoginUser.Text,textBoxLoginPassword.Text))
            {
                this.Hide();
            } 
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            textBoxLoginPassword.UseSystemPasswordChar = !checkBox1.Checked;
        }

        private void LoginForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }
    }
}
