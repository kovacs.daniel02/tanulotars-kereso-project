# Tanulótárs Manager

A Tanulótárs Manager egy Windows alkalmazás amely segít a kezelni a Tanulótárs weblaphoz tartozó adatokat

## Telepítés

1. Kicsomagoljuk a `Telepito.rar` file-t
2. Futtatjuk a `setup.exe` -t
3. A Telepítés gombra kattintva már fel is települ a program `TanuloTarsApp` néven

## Használat

A program indításakor **be** kell jelentkeznie a felhasználónak (``adminok.txt``)

![login form link](resources/loginForm.jpg)

Sikeres bejelentkezésnél megjelenik maga a *Student Manager* ablak

![main form link](resources/mainForm.jpg)

A táblázatban láthatjuk a diákokat és hozzá tartozó adatokat

---

* **Keresés** - `Search`-nél kiválasztjuk a lenyíló fülnél mire szeretnénk keresni, azután a keresett elem fog megjelenni a táblázatban ![main form link](resources/comboBox.jpg)

* **Frissítés** - a `Reload` gombra kattintva lefrissül a táblázat

* **Szerkeztés** - kiválasztjuk a szerkezteni kívánt diákot, elvégezzük a szerkeztést, majd az `Edit` gomb megnyomásával az adatbázisban is megtörténik a változtatás

* **Törlés** - `Delete` gomb megnyomásával a kiválasztott diák törlődni fog

* **Új hozzáadása** - `Add New`-ra kattintva a kitöltött mezők bekerülünek az adatbázisba mint új diák (a mezők gyors kiüritésére a `Clear` gomb szolgál)


## Fejlesztői információk

A program C# nyelven íródott, a [Visual Studio](https://visualstudio.microsoft.com/) fejlesztői felületet használva.

---

Az adat struktúra felépítésére az [Entity Framework Core](https://docs.microsoft.com/en-us/ef/core/) ORM keresztrendszert használtuk. A következő szerver konfigurációval:
``` protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        optionsBuilder.UseMySQL("
            server=46.101.185.21;
            database=daranyi_orm;
            user=*****;
            password=*****");
    }
```


