const express = require('express');
const app = express();
const mysql = require('mysql2');
const port = 3000

app.use(express.json());

// Incoming requests to serve from 'public' / Beérkező kéréseket kiszolgálja 'public'-ból
app.use('/', express.static("public"));


// Database connection / Adatbázis csatlakozás
const db = mysql.createPool({
    connectionLimit: 10,
    host: '46.101.185.21',
    user: 'szasz_edu',
    password: 'SzaszSQL2021',
    database: 'daranyi_orm',
});


// Read / Beolvasás
app.get('/students', (req, res) => {
    const sqlAllStudents = "SELECT * FROM Students";

    db.getConnection((err, connection) => {
        if (err) throw err;

        connection.query(sqlAllStudents, (err, result) => {
            connection.release()

            if (!err) {
                res.send(result)
            } else {
                console.log(err)
            }
        })
    })
});


// CREATE / Létrehozás
app.post('/students/add', (req, res) => {
    const newStudent = {
        Username: req.body.username,
        Age: req.body.age,
        Gender: req.body.gender,
        Location: req.body.location,
        Email: req.body.email,
        Degree: req.body.degree,
        Subject: req.body.subject,
        Language: req.body.language,
        AboutMe: req.body.aboutMe
    };

    const sqlInsertStudent = "INSERT INTO Students (Username, Age, Gender, Location, Email, Degree, Subject, Language, AboutMe) VALUES (?,?,?,?,?,?,?,?,?)";

    db.getConnection((err, connection) => {
        if (err) throw err;
        connection.query(sqlInsertStudent,
            [newStudent.Username,
            newStudent.Age,
            newStudent.Gender,
            newStudent.Location,
            newStudent.Email,
            newStudent.Degree,
            newStudent.Subject,
            newStudent.Language,
            newStudent.AboutMe], (err, result) => {
                connection.release()

                if (result.affectedRows) {
                    res.send(`Új felhasználó "${result.insertId}" ID alapján létre lett hozva.`)
                } else {
                    console.log(err)
                }
            })
    })
});


// UPDATE / Szerkesztés, felülírás
app.put('/students/:id', (req, res) => {
    const Id = req.params.id

    const Email = req.body.email
    const Degree = req.body.degree
    const Subject = req.body.subject
    const Language = req.body.language
    const AboutMe = req.body.aboutMe

    const sqlUpdateStudent = "UPDATE Students SET Email=?, Degree=?, Subject=?, Language=?, AboutMe=? WHERE Id=?";

    db.getConnection((err, connection) => {
        if (err) throw err;
        connection.query(sqlUpdateStudent, [Email, Degree, Subject, Language, AboutMe, Id], (err, result) => {
            connection.release()

            if (!err) {
                res.send(`A felhasználó "${Id}" ID alapján frissítve lett.`)
            } else {
                console.log(err)
            }
        })
    })
});


// DELETE / Törlés
app.delete('/students/:id', (req, res) => {
    const id = req.params.id;
    const sqlDeleteOneStudent = "DELETE FROM Students WHERE Id = ?";

    db.getConnection((err, connection) => {
        if (err) throw err;

        connection.query(sqlDeleteOneStudent, id, (err, result) => {
            connection.release()

            if (!err) {
                res.send(`A felhasználó "${id}" ID alapján törölve lett.`)
            } else {
                console.log(err)
            }
        })
    })
});


app.listen(port, () => {
    console.log(`A Tanulótárs oldal a következő címen fut: http://localhost:${port}`)
});