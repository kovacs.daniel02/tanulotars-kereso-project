let state = {
    students: [],
    editedId: ''
};

// Admin mode activation on icon click
let isAdmin = false;
document.getElementById('permission').onclick = function () {
    isAdmin = !isAdmin;
    if (isAdmin) {
        button = document.querySelectorAll('.admin')
        button.forEach((e) => {
            e.classList.remove('invisible');
        });
    } else {
        button = document.querySelectorAll('.admin');
        button.forEach((e) => {
            e.classList.add('invisible');
        });
    }
}

// On catalog page load, fetch & render students / Catalog oldalbetöltésnél megjeleníti a tanulók listáját
async function fetchAndRenderStudents() {
    const response = await fetch('/students');
    if (!response.ok) {
        alert('Szerver hiba');
        return;
    }
    state.students = await response.json();

    let studentsHTML = "";

    state.students.forEach((student) => {
        studentsHTML += `
        <div class="card bg-light border-secondary mb-4" style="max-width: 52rem;">
            <div class="card-header">
                <h5>${student.Username}</h5>
            </div>
            <div class="card-body">
                <ul class="list-group list-group-flush">
                    <li class="list-group-item bg-light"><b>Tanulandó tantárgy: </b>${student.Subject}</li>
                    ${student.Language ? `<li class="list-group-item bg-light"><b>Preferált nyelv: </b>${student.Language}</li>` : ""}
                    <li class="list-group-item bg-light"><b>Elérni kívánt végzettség: </b>${student.Degree}</li>
                    <li class="list-group-item bg-light"><b>Elérhetőség: </b>${student.Email}</li>
                    ${student.AboutMe ? `<li class="list-group-item bg-light"><b>Rólam: </b>${student.AboutMe}</li>` : ""}
                </ul>
                <button class="btn btn-danger float-right delete-student admin invisible" data-studentid="${student.Id}">
                    Törlés
                </button>
                <button class="btn btn-warning float-right edit-student admin invisible" data-studentid="${student.Id}">
                    Szerkesztés
                </button>
            </div>
        </div>`;
    });

    document.getElementById("student-list").innerHTML = studentsHTML;

    // Edit button/ szerkesztés gomb 
    for (let editBtn of document.querySelectorAll('.edit-student')) {
        editBtn.onclick = function (event) {
            let id = event.target.dataset.studentid;
            state.editedId = id;
            window.scrollTo(0, 0);
            renderEditStudent();
        }
    }

    // Delete Button / Törlés gomb
    for (let deleteBtn of document.querySelectorAll('.delete-student')) {
        deleteBtn.onclick = async function (event) {
            let id = event.target.dataset.studentid;

            const response = await fetch(`/students/${id}`, {
                method: 'DELETE'
            });

            if (!response.ok) {
                alert('Törlés sikertelen');
                return;
            }

            fetchAndRenderStudents();
        }
    }
}


// Form filling and submitting / Form kitöltése és elküldése
function renderEditStudent() {
    if (state.editedId === '') {
        document.getElementById('edit-student').innerHTML = '';
        return;
    }

    let foundStudent;
    for (let student of state.students) {
        if (student.Id == state.editedId) {
            foundStudent = student;
            break;
        }
    }

    let editFormHTML = `
        <form class="col-sm-12 rounded border border-secondary update" id="update-student">
            <h4>${foundStudent.Username}</h4>
            <!--EDIT Contact email / Email elérhetőség -->
            <div class="form-group">
                <label for="editedEmail">Email elérhetőség:*</label>
                <div>
                    <input type="email" class="form-control" id="editedEmail" value="${foundStudent.Email}" required>
                </div>
            </div>
            <!--EDIT Degree / Végzettsége-->
            <div class="form-group">
                <label for="editedDegree">Elérni kívánt végzettség:*</label>
                <div>
                    <select class="form-control" id="editedDegree" required>
                        <option selected value="${foundStudent.Degree}">${foundStudent.Degree}</option>
                        <optgroup label="MKKR 4.szint">
                            <option value="Érettségi">Érettségi</option>
                            <option value="Középfokú szakképesítés">Középfokú szakképesítés</option>
                            <option value="Középfokú szakképesítés-ráépülés">Középfokú
                                szakképesítés-ráépülés</option>
                            <option value="Felső középfokú részszakképesítés">Felső középfokú
                                részszakképesítés</option>
                            <option value="Felső középfokú szakképesítés">Felső középfokú szakképesítés
                            </option>
                            <option value="Felnőttképzési egyéb szakmai képzés">Felnőttképzési egyéb szakmai
                                képzés</option>
                        </optgroup>
                        <optgroup label="MKKR 5.szint">
                            <option value="Felső középfokú szakképesítés">Felső középfokú szakképesítés
                            </option>
                            <option value="Emelt szintű szakképesítés">Emelt szintű szakképesítés</option>
                            <option value="Emelt szintű szakképesítés-ráépülés">Emelt szintű
                                szakképesítés-ráépülés</option>
                            <option value="Felsőoktatási szakképzés">Felsőoktatási szakképzés</option>
                            <option value="Felnőttképzési egyéb szakmai képzés">Felnőttképzési egyéb szakmai
                                képzés</option>
                        </optgroup>
                        <optgroup label="MKKR 6.szint">
                            <option value="Felsőfokú végzettséghez kötött szakképesítés">Felsőfokú
                                végzettséghez kötöttszakképesítés</option>
                            <option value="Felsőoktatási alapfokozat (BA/BSc)">Felsőoktatási alapfokozat
                                (BA/BSc)</option>
                            <option value="Szakirányú továbbképzés">Szakirányú továbbképzés</option>
                            <option value="Felnőttképzési egyéb szakmai képzés">Felnőttképzési egyéb szakmai
                                képzés</option>
                        </optgroup>
                        <optgroup label="MKKR 7.szint">
                            <option value="Felsőoktatási mesterfokozat (MA/MSc)">Felsőoktatási mesterfokozat
                                (MA/MSc)</option>
                            <option value="Szakirányú továbbképzés">Szakirányú továbbképzés</option>
                            <option value="Felnőttképzési egyéb szakmai képzés">Felnőttképzési egyéb szakmai
                                képzés</option>
                        </optgroup>
                        <optgroup label="MKKR 8.szint">
                            <option value="Doktori fokozat (PhD/DLA)">Doktori fokozat (PhD/DLA)</option>
                        </optgroup>
                    </select>
                </div>
            </div>
            <!--EDIT Subject / Tantárgy -->
            <div class="form-group">
                <label for="editedSubject">Tanulni kívánt tantárgy:*</label>
                <div>
                    <select class="form-control" id="editedSubject" required>
                        <option selected value="${foundStudent.Subject}">${foundStudent.Subject}</option>
                        <option value="Informatika">Informatika</option>
                        <option value="Idegen nyelv">Idegen nyelv</option>
                        <option value="Irodalom">Irodalom</option>
                        <option value="Történelem">Történelem</option>
                    </select>
                </div>
            </div>
            <!--EDIT Preferred language / Preferált nyelv -->
            <div class="form-group">
                <label for="editedLanguage">Preferált nyelv:</label>
                <div>
                    <input type="text" class="form-control" id="editedLanguage" autocomplete="off"
                        value="${foundStudent.Language}"">
                </div>
            </div>
            <!--EDIT About me / Róvid leírás magadról-->
            <div class=" form-group">
                    <label for="editedAboutMe">Pár szót röviden magadról:</label>
                    <div>
                        <textarea class="form-control" id="editedAboutMe" rows="3">${foundStudent.AboutMe}</textarea>
                    </div>
                </div>
                <small class="form-text text-muted">A csillaggal (*) jelölt mezők kitöltése kötelező.</small>
                <button class="btn btn-warning edit-student" type="submit">Küldés</button>
        </form>
    `;

    document.getElementById('edit-student').innerHTML = editFormHTML;

    document.getElementById('update-student').onsubmit = async function (event) {
        event.preventDefault();
        let email = event.target.elements.editedEmail.value;
        let degree = event.target.elements.editedDegree.value;
        let subject = event.target.elements.editedSubject.value;
        let language = event.target.elements.editedLanguage.value;
        let aboutMe = event.target.elements.editedAboutMe.value;

        const body = {
            email: email,
            degree: degree,
            subject: subject,
            language: language,
            aboutMe: aboutMe
        };

        const res = await fetch(`/students/${state.editedId}`, {
            method: 'PUT',
            body: JSON.stringify(body),
            headers: {
                'content-type': 'application/json',
            }
        })

        if (!res.ok) {
            alert('Szerver hiba');
            return;
        }

        state.editedId = '';

        fetchAndRenderStudents();
        renderEditStudent();
    }
}


window.onload = fetchAndRenderStudents;