// Navbar fade in on scroll
function toggleScrollClass() {
    let nav = document.querySelectorAll('.navbar')[0];
    window.pageYOffset > 0 ? nav.classList.remove('fade') : nav.classList.add('fade')
}
window.addEventListener('scroll', function () { toggleScrollClass() });
toggleScrollClass();