// Form filling and submitting / Form kitöltése és elküldése
document.getElementById('create-student').onsubmit = async function (event) {
    event.preventDefault();
    let username = event.target.elements.username.value;
    let age = dateToAge(event.target.elements.age.value);
    let gender = event.target.elements.gender.value;
    let location = event.target.elements.location.value;
    let email = event.target.elements.email.value;
    let degree = event.target.elements.degree.value;
    let subject = event.target.elements.subject.value;
    let language = event.target.elements.language.value;
    let aboutMe = event.target.elements.aboutMe.value;

    const body = {
        username: username,
        age: age,
        gender: gender,
        location: location,
        email: email,
        degree: degree,
        subject: subject,
        language: language,
        aboutMe: aboutMe
    };

    const res = await fetch('/students/add', {
        method: 'POST',
        headers: {
            'content-type': 'application/json',
        },
        body: JSON.stringify(body)
    })

    if (!res.ok) {
        alert('Létrehozás sikertelen');
        return;
    }

    window.location.href = "../html/catalog.html";
};




// html input type="date" to -> age(number)
function dateToAge(dateString) {
    let birthday = +new Date(dateString);
    return ~~((Date.now() - birthday) / (31557600000));
}