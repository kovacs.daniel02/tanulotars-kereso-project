# Tanulótárs kereső project

## A szoftver célja és leirása 

A webes applikáció által a felhasználó tanulási céljainak és igényeinek megfelelően tanulótársat kereshet magának.

- **A főoldalon** a kliens 2 oldal között navigálhat. A "Tanulótársat keresek" feliratú gombra kattintva a "form" oldalra jutunk, míg a "Böngészés a tanulók között" gombra kattintva "catalag" oldalra jutunk.

![web home link](Images/home.png)

- **A "form" oldalon** a felhasználó kitölthet bizonyos adatokat, például elérni kívánt végzettség , email cim, preferált nyelv, stb. A kitöltés után a feltöltés gombra kattintva az alkalmazás átnavigál minket a “catalog” oldalra, ahol böngészhetünk a többi tanuló között, aki kitöltötte az ürlapot.

![web form link](Images/form.png)

- **A "catalog" oldalon** megjelennek a tanulók adatai, és az admin gombra kattintva lehetőség van a tanulók törlésére és szerkesztésére is.

![web catalog link](Images/catalog.png)

***
## Az applikáció technikai leírása:


A webes alkalmazás egy adatbázis kapcsolattal rendelkezik, amelyhez a kliens oldalon lehet hozzáadni, törölni és szerkeszteni elemeket. Az adatbázis létrehozása a `MySQL Workbench` programmal történt.

A Szerver létrehozásához a `Node.js` szoftverrendszert használtuk.

A következő npm könyvtárakat használtuk fel:

- `mysql2`
- `express`
- `nodemon`

Frontend oldalon a megjelenítést és a funkcionalitást `HTML`, `CSS` és `JavaScript` fájlok segitségével hoztuk létre.
